# Intro
This script allows to generate the files related to the hexagonal architecture

# Generated files
* Application/UseCase
* Application/Response
* Application/Request
* Application/Presenter
------
* UserInterface/Presenter
* UserInterface/ViewModel

You can choose which files are generated or not

# Configuration
Line 9 you can define the path from the root to define a special path

# How 
To use the script you have to run the command 'php console.php useCase'.
