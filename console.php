<?php
$test = new MakeHexa();
$test->execute($argc, $argv);


class MakeHexa
{
    //To be filled in if you don't want the file generation to be done by root
    const ROOT_PATH = "";

    public function execute($argc,$argv)
    {
        $filters = $this->getFilters($argc, $argv);
        if($filters[0] == "help" || empty($filters[0]))
        {
            $this->help();
        }
        if(self::ROOT_PATH == ""){
            echo "\e[31mWarning : You have not filled the absolute path constant\e[39m \n";
        }
        $method = $filters[0];
        if(method_exists($this, $method))
        {
            $this->$method();
        }
        $this->help();
    }

    /**
     * @param int $argc
     * @param array $argv
     * @return array
     */
    private function getFilters(int $argc, array $argv): array
    {
        $filters = array_fill(0, 3, null);
        for($i = 1; $i < $argc; $i++) {
            $filters[$i - 1] = $argv[$i];
        }
        return $filters;
    }

    /**
     * Function that displays the help
     *
     * @return void
     */
    private function help()
    {
        echo "Available commands: \n";
        echo "  useCase           Create a new useCase \n";
        die();
    }

    /**
     * Function that creat a useCase
     *
     * @return void
     */
    private function useCase()
    {
        $useCaseName= readline("UseCase name? : ");
        $applicationFiles = $this->getApplicationFiles();
        $userInterfaceFiles = $this->getUserInterfaceFiles();
        $this->createFiles($useCaseName, $applicationFiles, $userInterfaceFiles);
        die();
    }

    /**
     * @return bool[]
     */
    private function getApplicationFiles(): array
    {
        echo "Application files : \n";
        $request = $this->getRequest();
        $response = $this->getResponse();
        $presenterInterface = $this->getPresenterInterface();
        return array(
            "Request" => $request,
            "Response" => $response,
            "PresenterInterface" => $presenterInterface
        );
    }

    /**
     * @return bool
     */
    private function getResponse(): bool
    {
        $default = "y";
        $fileName = "Response";
        $response = $this->getUserResponse($fileName, $default);

        return $response;
    }

    /**
     * @return bool
     */
    private function getRequest(): bool
    {
        $default = "y";
        $fileName = "Request";
        $request = $this->getUserResponse($fileName, $default);

        return $request;
    }

    /**
     * @return bool
     */
    private function getPresenterInterface(): bool
    {
        $default = "y";
        $fileName = "PresenterInterface";
        $presenterInterface = $this->getUserResponse($fileName, $default);

        return $presenterInterface;
    }

    /**
     * @param string $fileName
     * @param string $default
     * @return bool
     */
    private function getUserResponse(string $fileName, string $default): bool
    {
        $UserResponse = readline("Do you want ".$fileName." file ? :[".$default."] ");
        $UserResponse = $this->repeate($UserResponse, "Do you want ".$fileName." file ? :[".$default."] ");
        $UserResponseFormated = $this->getUseResponseFormated($UserResponse, $default);

        return $UserResponseFormated;
    }

    /**
     * @return bool[]
     */
    private function getUserInterfaceFiles(): array
    {
        echo "UserInterface files : \n";
        $presenter = $this->getPresenter();
        $viewModel = $this->getViewModel();
        return array(
            "Presenter" => $presenter,
            "ViewModel" => $viewModel
        );
    }

    /**
     * @return bool
     */
    private function getPresenter(): bool
    {
        $default = "y";
        $fileName = "Presenter";
        $presenter = $this->getUserResponse($fileName, $default);

        return $presenter;
    }

    /**
     * @return bool
     */
    private function getViewModel(): bool
    {
        $default = "y";
        $fileName = "ViewModel";
        $viewModel = $this->getUserResponse($fileName, $default);

        return $viewModel;
    }

    /**
     * @param string $userResponse
     * @param string $line
     * @return string
     */
    private function repeate(string $userResponse, string $line): string
    {
        while($userResponse != "y" &&
              $userResponse != "n" &&
              $userResponse != "yes" &&
              $userResponse != "no" &&
              $userResponse != ""){
            $userResponse = readline($line);
        }

        return $userResponse;
    }

    /**
     * @param string $userResponse
     * @param string $default
     * @return bool
     */
    private function getUseResponseFormated(string $userResponse, string $default):bool
    {
        $userResponseFormated = false;

        if($userResponse == ""){
            $userResponse = $default;
        }

        if($userResponse == "y" || $userResponse == "yes"){
            $userResponseFormated = true;
        }

        return $userResponseFormated;
    }

    /**
     * @param string $useCaseName
     * @param array $applicationFiles
     * @param array $userInterfaceFiles
     * @return void
     */
    private function createFiles(string $useCaseName, array $applicationFiles, array $userInterfaceFiles)
    {
        $this->createUseCaseFile($useCaseName);
        if($applicationFiles['Response']){
            $this->createResponseFile($useCaseName);
        }
        if($applicationFiles['Request']){
            $this->createRequestFile($useCaseName);
        }
        if($applicationFiles['PresenterInterface']){
            $this->createPresenterInterfaceFile($useCaseName);
        }
        if($userInterfaceFiles['Presenter']){
            $this->createPresenterFile($useCaseName);
        }
        if($userInterfaceFiles['ViewModel']){
            $this->createViewModelFile($useCaseName);
        }


    }

    /**
     * @param string $useCaseName
     * @return void
     */
    private function createUseCaseFile(string $useCaseName)
    {
        $path = "Application/Usecase";
        $useCaseNameExplode = explode("/",$useCaseName);
        $useCaseName = end($useCaseNameExplode);
        $path = $this->getPath($useCaseNameExplode, $path);
        if(!is_dir($path)){
            mkdir($path,0777, true);
        }

        file_put_contents($path."/".$useCaseName."UseCase.php",
            $this->getContentForUseCaseFile($useCaseName));

    }

    /**
     * @param string $useCaseName
     * @return string
     */
    private function getContentForUseCaseFile(string $useCaseName): string
    {
        $content = "<?php
           
class ".$useCaseName."UseCase 
{
    public function __construct()
    {
    }
    
    public function run()
    {
    }
}";

        return $content;
    }

    /**
     * @param string $useCaseName
     * @return void
     */
    private function createResponseFile(string $useCaseName)
    {
        $path = "Application/Response";
        $useCaseNameExplode = explode("/",$useCaseName);
        $useCaseName = end($useCaseNameExplode);
        $path = $this->getPath($useCaseNameExplode, $path);
        if(!is_dir($path)){
            mkdir($path,0777, true);
        }

        file_put_contents($path."/".$useCaseName."Response.php",
            $this->getContentForResponseFile($useCaseName));

    }

    /**
     * @param string $useCaseName
     * @return string
     */
    private function getContentForResponseFile(string $useCaseName): string
    {
        $content = '<?php

class '.$useCaseName.'Response
{

    private $response;


    public function __construct($response)
    {
        $this->response = $response;
    }


    public function getResponse()
    {
        return $this->response;
    }
}';

        return $content;
    }

    /**
     * @param string $useCaseName
     * @return void
     */
    private function createRequestFile(string $useCaseName)
    {
        $path = "Application/Request";
        $useCaseNameExplode = explode("/",$useCaseName);
        $useCaseName = end($useCaseNameExplode);
        $path = $this->getPath($useCaseNameExplode, $path);
        if(!is_dir($path)){
            mkdir($path,0777, true);
        }

        file_put_contents($path."/".$useCaseName."Request.php",
            $this->getContentForRequestFile($useCaseName));

    }

    /**
     * @param string $useCaseName
     * @return string
     */
    private function getContentForRequestFile(string $useCaseName): string
    {
        $content = '<?php

class '.$useCaseName.'Request
{
    /** @var array */
    private $parameters;

    /**
     * @param array $parameters
     */
    public function __construct(array $parameters)
    {
        $this->parameters = $parameters;
    }
}';

        return $content;
    }

    /**
     * @param string $useCaseName
     * @return void
     */
    private function createPresenterInterfaceFile(string $useCaseName)
    {
        $path = "Application/Presenter";
        $useCaseNameExplode = explode("/",$useCaseName);
        $useCaseName = end($useCaseNameExplode);
        $path = $this->getPath($useCaseNameExplode, $path);
        if(!is_dir($path)){
            mkdir($path,0777, true);
        }

        file_put_contents($path."/".$useCaseName."PresenterInterface.php",
            $this->getContentForPresenterInterfaceFile($useCaseName));

    }

    /**
     * @param string $useCaseName
     * @return string
     */
    private function getContentForPresenterInterfaceFile(string $useCaseName): string
    {
        $content = '<?php

interface '.$useCaseName.'PresenterInterface
{
    public function present('.$useCaseName.'Response $response);
}';

        return $content;
    }

    /**
     * @param string $useCaseName
     * @return void
     */
    private function createPresenterFile(string $useCaseName)
    {
        $path = "UserInterface/Presenter";
        $useCaseNameExplode = explode("/",$useCaseName);
        $useCaseName = end($useCaseNameExplode);
        $path = $this->getPath($useCaseNameExplode, $path);
        if(!is_dir($path)){
            mkdir($path,0777, true);
        }

        file_put_contents($path."/".$useCaseName."Presenter.php",
            $this->getContentForPresenterFile($useCaseName));

    }

    /**
     * @param string $useCaseName
     * @return string
     */
    private function getContentForPresenterFile(string $useCaseName): string
    {
        $content = '<?php

class '.$useCaseName.'Presenter implements '.$useCaseName.'PresenterInterface
{
    /** @var '.$useCaseName.'ViewModel */
    private $'.lcfirst($useCaseName).'ViewModel;

    public function present('.$useCaseName.'Response $response)
    {
        $this->'.lcfirst($useCaseName).'ViewModel = new '.$useCaseName.'ViewModel($response->getResponse());
    }

    public function get'.$useCaseName.'ViewModel(): '.$useCaseName.'ViewModel
    {
        return $this->'.lcfirst($useCaseName).'ViewModel;
    }
}';

        return $content;
    }

    /**
     * @param string $useCaseName
     * @return void
     */
    private function createViewModelFile(string $useCaseName)
    {
        $path = "UserInterface/ViewModel";
        $useCaseNameExplode = explode("/",$useCaseName);
        $useCaseName = end($useCaseNameExplode);
        $path = $this->getPath($useCaseNameExplode, $path);
        if(!is_dir($path)){
            mkdir($path,0777, true);
        }

        file_put_contents($path."/".$useCaseName."ViewModel.php",
            $this->getContentForViewModelFile($useCaseName));

    }

    /**
     * @param string $useCaseName
     * @return string
     */
    private function getContentForViewModelFile(string $useCaseName): string
    {
        $content = '<?php

class '.$useCaseName.'ViewModel
{
    private $response;


    public function __construct($response)
    {
        $this->response = $response;
    }
}';

        return $content;
    }

    /**
     * @param array $useCaseNameExplode
     * @param string $path
     * @return string
     */
    private function getPath(array $useCaseNameExplode, string $path): string
    {
        for ($i = 0; $i < count($useCaseNameExplode) - 1; $i++)
        {
            $path = $path.'/'.$useCaseNameExplode[$i];
        }

        return self::ROOT_PATH.$path;
    }
}
